<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/estilorp.css" />

<title>Insercin Cliente</title>
</head>
<body>
	<div id="cabecera">
		<div id="logotipo">
			<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniera Telemtica" />
			</a>
		</div>
	</div>

	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
		<h1> Error al insertar el nuevo cliente </h1>
	</div>

</body>
</html>