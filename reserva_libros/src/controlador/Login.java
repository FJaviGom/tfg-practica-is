package controlador;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Cliente;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String cliente = request.getParameter("cliente");
//		String contrasena = request.getParameter("contrasena");

		String cliente = null;
		String contrasena = null;
		
		boolean control = true;
		
		boolean b_cliente = true;
		boolean b_contrasena = true;
		
		// Comprobacion de que el campo cliente no sea vaco 
		if(request.getParameter("cliente") == null || request.getParameter("cliente")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo cliente no puede estar vacio");

			b_cliente = false;
		}	
		else {
			cliente = request.getParameter("cliente");
		}	
		// Comprobacion de que el campo cliente tenga entre 5 y 12 caracteres
		if(b_cliente == true){
			if(cliente.length() < 5 || cliente.length() > 12){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo cliente tiene que estar entre 5 y 12 caracteres");
	      		
	      		control = false;
//	       		request.getRequestDispatcher("inicio_tras_error.jsp").forward(request, response);	
			}
		}        

		// Comprobacion de que el campo contrasea no sea vaco 
		if(request.getParameter("contrasena") == null || request.getParameter("contrasena")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo contrasena no puede estar vacio");

			b_contrasena = false;
		}	
		else {
			contrasena = request.getParameter("contrasena");
		}	
		// Comprobacion de que el campo contrasena tenga entre 5 y 12 caracteres
		if(b_contrasena == true){
			if(contrasena.length() < 5 || contrasena.length() > 12){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo contrasena tiene que estar entre 5 y 12 caracteres");
	      		
	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);	
			}
		}        

	    if (control==true){			
	    	Cliente cliente_login = new Cliente();
		//podriamos haber creado el constructor con cliente y contrasena y nos quitamos el meterlos asi
		
/*
		// Creado para poder enviar el dni del cliente que quiere realizar la reserva
		Integer dni_cliente_activo = cliente_login.getDni();		
		HttpSession sesion = request.getSession(); 
		sesion.setAttribute("dni_cliente_activo", dni_cliente_activo);
*/		
	    	if(cliente_login.login(cliente, contrasena) > 0){
	    		request.getRequestDispatcher("datos.jsp").forward(request, response);

/*			//Para obtener dni de usuario con la cuenta activa para reservas
  			HttpSession sesion_activa = request.getSession(); 
			sesion_activa.setAttribute("dni_cliente_activo", cliente_login.getDni());
*/
			}
	    	else {
		//	alert("Error en el usuario o contrasea. Por favor, introdzcalos correctamente.");
	    		request.getRequestDispatcher("inicio_tras_error.jsp").forward(request, response);			
	    	}
	    }
	    else { //if(control==false)
	    	request.getRequestDispatcher("inicio_tras_error.jsp").forward(request, response);
	    }
	}
}
