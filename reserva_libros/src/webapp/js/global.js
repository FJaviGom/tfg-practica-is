/**** Fichero usado para definir las variables globales al resto de ficheros js ****/

var min_cont = 5;
var max_cont = 12;
var min_client = 5;
var max_client = 12;

var min_dni = 8;
var max_dni = 8;
var max_nombre = 20;
var max_apellido = 20;
var max_direccion = 40;

// Variables relacionadas con Libro
var long_isbn = 10;

var max_titulo = 50;
var max_autor = 50;
var max_editorial = 50;

var max_digitos_anio = 4;