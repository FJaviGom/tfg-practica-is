package unidad;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

import modelo.NuevoCliente;
import modelo.Conexion;

public class NuevoClienteTest {

	@Test
	public final void testConstructor1NuevoClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 1 del modelo NuevoCliente funciona bien");

		NuevoCliente nuevoCliente = new NuevoCliente();
	}

	@Test
	public final void testConstructor2NuevoClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 3 del modelo NuevoCliente funciona bien");

		Integer dni = 111111;
		String usuario = "Usuario1";
		String nombre = "Nombre";
		String apellido1 = "Primer apellido";
		String apellido2 = "Segundo apellido";
		String direccion = "Dirección del nuevoCliente";
		Boolean admin = true;
		String contrasena = "contraseña";

		NuevoCliente nuevoCliente;

    nuevoCliente = new NuevoCliente(dni, usuario, nombre, apellido1, apellido2, direccion, admin, contrasena);

		assertEquals("El dni debería haberse asignado correctamente", nuevoCliente.getDni(), dni);
		assertEquals("El nuevoCliente debería haberse asignado correctamente", nuevoCliente.getCliente(), usuario);
		assertEquals("El nombre debería haberse asignado correctamente", nuevoCliente.getNombre(), nombre);
		assertEquals("El apellido1 debería haberse asignado correctamente", nuevoCliente.getApellido1(), apellido1);
		assertEquals("El apellido2 debería haberse asignado correctamente", nuevoCliente.getApellido2(), apellido2);
		assertEquals("La direccion debería haberse asignado correctamente", nuevoCliente.getDireccion(), direccion);
		assertEquals("El valor admin debería haberse asignado correctamente", nuevoCliente.getAdmin(), admin);
		assertEquals("La contrasena debería haberse asignado correctamente", nuevoCliente.getContrasena(), contrasena);
	}

	@Test
	public final void testConstructor3NuevoClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 4 del modelo NuevoCliente funciona bien");

		Integer dni = 111111;
		String nombre = "Nombre";
		String apellido1 = "Primer apellido";
		String apellido2 = "Segundo apellido";
		String direccion = "Dirección del nuevoCliente";
		Boolean admin = true;

		NuevoCliente nuevoCliente;

    nuevoCliente = new NuevoCliente(dni, nombre, apellido1, apellido2, direccion, admin);

		assertEquals("El dni debería haberse asignado correctamente", nuevoCliente.getDni(), dni);
		assertEquals("El nombre debería haberse asignado correctamente", nuevoCliente.getNombre(), nombre);
		assertEquals("El apellido1 debería haberse asignado correctamente", nuevoCliente.getApellido1(), apellido1);
		assertEquals("El apellido2 debería haberse asignado correctamente", nuevoCliente.getApellido2(), apellido2);
		assertEquals("La direccion debería haberse asignado correctamente", nuevoCliente.getDireccion(), direccion);
		assertEquals("El valor admin debería haberse asignado correctamente", nuevoCliente.getAdmin(), admin);
	}
}
