
//Valida el formulario de insertar

function compr_nuevo_libro(){

	var isbn = document.getElementById("isbn").value;
	var titulo = document.getElementById("titulo").value;
	var autor = document.getElementById("autor").value;
	var editorial = document.getElementById("editorial").value;
	var anio_pub = document.getElementById("anio_pub").value;
	var genero = document.getElementById("genero").value;
	var observaciones = document.getElementById("observaciones").value;
/*	
	var fecha_actual = new Date();
	var anio_actual = fecha.getFullYear();
*/	
	var resultado = true;

	/******************************/
	/******* Comprobaciones *******/
	/******************************/
	
	/****** Comprobaciones sobre campo isbn ******/
	/** Comprobamos que el campo ISBN no sea vacío */
	if(resultado == true){
		resultado = compruebaVacio(isbn,"ISBN");
		if (resultado == true){
			/** Si el campo ISBN no es vacío, comprobamos que el campo ISBN tenga 10 caracteres numéricos */
			resultado = compruebaISBN(isbn,long_isbn,"ISBN");
			}
		}
	
	/****** Comprobaciones sobre campo titulo ******/
	if (resultado == true){
		/** Comprobamos que el campo Titulo no sea vacío */
		resultado = compruebaVacio(titulo,"Título");
		if (resultado == true){
			/** Comprobamos el tamaño máximo del título (para controlar los datos introducidos) */
			resultado = compruebaMax(titulo,max_titulo,"titulo");	
			}
		}
	
	/****** Comprobación sobre campo autor ******/
	if (resultado == true){
		/** Comprobamos que el campo Autor no sea vacío */
		resultado = compruebaVacio(autor,"Autor");
		if (resultado == true){
			/** Comprobamos el tamaño máximo del título (para controlar los datos introducidos) */
			resultado = compruebaMax(autor,max_autor,"autor");	
			}
		}	

	/****** Comprobación sobre campo editorial ******/
	if (resultado == true){
		/** Comprobamos que el campo Editorial no sea vacío */
		resultado = compruebaVacio(editorial,"Editorial");
		if (resultado == true){
			/** Comprobamos el tamaño máximo del título (para controlar los datos introducidos) */
			resultado = compruebaMax(editorial,max_editorial,"editorial");	
			}
		}	
	
	/****** Comprobación sobre campo anio_pub ******/
	if (resultado == true){
		/** Comprobamos que el campo Año de publicación no sea vacío */
		resultado = compruebaVacio(anio_pub,"Año de publicación");
		if (resultado == true){
			/** Comprobamos el tamaño máximo del título (para controlar los datos introducidos) */
			resultado = compruebaAnioPub(anio_pub,max_digitos_anio,"Año de publicacion");	
	/*		if (resultado == true){
				//Comprobamos que el año introducido es menor que el año actual
				resultado = compruebaAnioCorrecto(anio_pub);
				}	*/
			}
		}	

	/****** Comprobación sobre campo genero ******/
	if (resultado == true){
		/** Comprobación de selección de un género de la lista */
		if(genero == "0" || genero == ''){
			genero = "";
			resultado = false;
			alert("Seleccione un género");
			}			
		}
	
	// Comprobación vacío año de publicación
//	resultado = compruebaVacio(anio_pub,"Año de publicación");
	
	// Comprobación vacío genero
//	resultado = compruebaVacio(genero,"género");
	

	/*** Comprobación de isbn ***/
	// Comprobación de que ISBN es un número
	// Trataremos el ISBN como un código numérico de 10 dígitos (formato ISBN hasta 2007)
//	resultado = compruebaEsNumerico(isbn,long_isbn,"ISBN");
//	if (resultado == true) {
	// Comprobación del número de dígitos del ISBN
//		resultado = compruebaLongitud(isbn,min_isbn,max_isbn,"ISBN");
//	}
	
	/*** Comprobación de título ***/	
	// Comprobamos el tamaño máximo del título (para controlar los datos introducidos)
//	resultado = compruebaMax(titulo,max_titulo,"titulo");
	
	/*** Comprobación de autor ***/	
	// Comprobamos el tamaño máximo del autor (para controlar los datos introducidos)
//	resultado = compruebaMax(autor,max_autor,"autor");
	
	/*** Comprobación de editorial ***/	
	// Comprobamos el tamaño máximo de la editorial (para controlar los datos introducidos)
//	resultado = compruebaMax(editorial,max_editorial,"editorial");
	
	/*** Comprobación de año de publicación ***/
	// Comprobamos que los caracteres introducidos son numéricos

//	resultado = compruebaEsNumerico(anio_pub);
/*	if (resultado == true) {
		// Comprobamos el tamaño máximo del año de publicación (para controlar los datos introducidos)
		resultado = compruebaMax(anio_pub,max_digitos_anio,"año de publicacion");	
		if (resultado == true){
			// Comprobamos si el año introducido es menor que el año actual
			if(anio_pub > anio_actual){
				alert('[ERROR] El año de publicacion introducido es incorrecto. Por favor, introduzca un año correcto.');
			}
		}
	}
	
	// Comprobación de selección de un género de la lista
	if(genero == "0" || genero == ''){
		genero = "";
		resultado = false;
		alert("Seleccione un género");
	}
*/
	return resultado;
}