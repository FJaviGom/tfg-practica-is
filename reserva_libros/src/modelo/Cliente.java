package modelo;

import java.sql.*;
import java.util.*;

import reserva_libros.Constantes;

import modelo.Conexion;

/**
 * Clase que almacena los datos de un usuario.
 * Pensado para usarse en el ámbito de sesión. 
 * Además es capaz de comprobar si el usuario/clave existe
 * en la base de datos. El uso habitual sería:
 *  - Inicialmente, se crea un objeto en la sesión.
 *  - Se da valores a las propiedades usuario y clave.
 *  - Se llama a getCorrecto(). Esta función buscará en la 
 *    base de datos si el usuario/clave existe. Si lo encuentra
 *    rellena las propiedades nombre y administrador con la información
 *    almacenada en la base de datos y establece correcto a verdadero.
 *    Una vez encontrado que el usuario es correcto, ya no se vuelve
 *    a comprobar más.
 */
public class Cliente {

	/**
	 * ID del usuario. Propiedad de solo lectura (solo get).
	 * El valor se obtiene de la base de datos cuando se llama
	 * a getCorrecto().
	 */
//	private int idc;

	/**
	 * Nombre completo real del usuario. Propiedad de solo lectura (get).
	 * El valor se obtiene de la base de datos cuando se llama
	 * a getCorrecto().
	 */
//	private Integer dni;
	protected Integer dni;
	/**
	 * Login del usuario. El utilizado en el formulario de
	 * entrada. Propiedad de lectura/escritura (get/set).
	 */
/*
	private String cliente;
	
	private String nombre;
	
	private String apellido1;
	
	private String apellido2;
	
	private String direccion;	
*/
	
	protected String cliente;
	
	protected String nombre;
	
	protected String apellido1;
	
	protected String apellido2;
	
	protected String direccion;
	
	/**
	 * Indica si este usuario es un administrador. Propiedad de solo lectura (get).
	 * El valor se obtiene de la base de datos cuando se llama
	 * a getCorrecto(). Será falso si es un usuario normal.
	 */
//	private Boolean admin;
	protected Boolean admin;		
	/**
	 * Clave del usuario. El utilizado en el formulario de
	 * entrada. Propiedad de lectura/escritura (get/set).
	 */
//	private String contrasena;
	protected String contrasena;
	
	Conexion obj_conn;
	
	
	/**
	 * 
	 * Indica si este usuario es correcto. Propiedad de solo lectura (get).
	 * Si es falso, el usuario no existe y no está autorizado.
	 */	
//	private boolean correcto;
	protected boolean correcto;
	
	/**
	 * Crea un usuario y da valores iniciales.
	 */
	public Cliente() {
		correcto=false;
		admin=false;
		
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
	}
	
	public Cliente(Integer dni) {
		this.dni = dni;
		
		correcto=false;
		admin=false;
		
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
	}
	
	public Cliente(Integer dni, String cliente, String nombre, String apellido1, String apellido2, String direccion,
			 Boolean admin, String contrasena) {
		this.dni = dni;
		this.cliente = cliente;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.direccion = direccion;
		this.admin = admin;		
		this.contrasena = contrasena;
		
		correcto=false;
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
		
	}
	
	public Cliente(Integer dni, String nombre, String apellido1, String apellido2, String direccion,
			Boolean admin) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.direccion = direccion;
		this.admin = admin;
		
		correcto=false;
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
		
	}
			
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public Integer getDni() {
		return dni;
	}
	public void setDni(Integer dni) {
		this.dni=dni;
	}	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre=nombre;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1=apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2=apellido2;
	}	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion=direccion;
	}	
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
/*	public int getIdc() {
		return idc;
	}	*/
	public Boolean getAdmin() {
		return admin;
	}
	
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	
	
	/**
	 * Determina si el cliente es un usuario vlido.
	 * Mientras la propiedad "correcto" sea falsa, cada vez que se llame
	 * a este mtodo se conectar a la base de datos, buscará si existe
	 * un usuario con los campos cliente y contrasea iguales a los de las 
	 * propiedades cliente y contrasea. Si lo encuentra, rellena el resto de 
	 * propiedades del bean y establece correcto a True.
	 * Si se produce algn error/excepcin o no se encuentra el cliente,
	 * la propiedad correcto no se modificar y seguir valiendo False.
	 * Si la variable correcto es True, no se hace nada y solo se devuelve True.
	 * @return True si el cliente est en la base de datos.
	 */
	public boolean getCorrecto() {
		if (!correcto) {
			try {
				// Verificamos en la base de datos
				Class.forName(Constantes.DB_DRIVER).newInstance();
				Connection conn = DriverManager.getConnection(Constantes.DB_URL,
						Constantes.DB_USER, Constantes.DB_PASS);

				Statement st = conn.createStatement();
				String consulta = "SELECT dni, nombre, apellido1, apellido2, direccion, admin" +
						" FROM clientes WHERE cliente='" + cliente +
						"' AND contrasena='" + contrasena + "';";
				ResultSet rs = st.executeQuery(consulta);
				if (rs.next()) {
					dni = rs.getInt("dni");
					nombre = rs.getString("nombre");
					apellido1 = rs.getString("apellido1");
					apellido2 = rs.getString("apellido2");
					direccion = rs.getString("direccion");
					admin = rs.getBoolean("admin");
					//Si hemos encontrado al menos una lnea, es correcto
					correcto=true;
				}
				// Liberamos los recursos
				rs.close();
				st.close();
				conn.close();

			} catch (SQLException e) {
				//Usuario continua siendo inválido
				e.printStackTrace();
			} catch (Exception e) {
				//Usuario continua siendo inválido
				//Driver no encontrado
				e.printStackTrace();
			} 
		}
		
		return correcto;
	}
	
	public Integer login (String c, String p){
		int resultado = 0;
		
		try {
		
			String consultaSql = "SELECT dni, nombre, apellido1, apellido2, direccion, admin" +
					" FROM clientes WHERE cliente='" + c +
					"' AND contrasena='" + p + "';";
			
		resultado = obj_conn.consultar(consultaSql);
		return resultado;
		} catch (SQLException e) {
			obj_conn.cerrarConexion();
					//e.printStackTrace();
		}
		return 0;
	}
	
	public Integer insertar_Cliente(){
		//Verificamos que estamos en una sesion, usuario administrador
			
//			Cliente_Bean cliente = new Cliente_Bean();
//			int resultado = -1;
			
//			if(cliente.getCorrecto() && cliente.getAdmin()) {

			//    int resultado = -1;
			    //Nos conectamos a la base de datos
		
		int resultado = 0;
		
		try {
			
		byte admin_byte = (byte)(admin?1:0);	
		
		String consultaSql = "INSERT INTO clientes (dni, cliente, nombre, apellido1, apellido2, direccion, admin, contrasena) VALUES ('"+
			    				dni+"', '"+
			    				cliente+"', '"+
			    				nombre+"', '"+
			    				apellido1+"', '"+
			    				apellido2+"', '"+
			    				direccion+"', '"+
			    				admin_byte+"', '"+
			    				contrasena+"');";
			
		resultado = obj_conn.insertar(consultaSql);
		} catch (SQLException e) {
			obj_conn.cerrarConexion();
			return -1;   //Se est intentando registrar una PK duplicada
					//e.printStackTrace();
				}
		obj_conn.cerrarConexion();
		return resultado;
	}
	
	public ArrayList<Cliente> listar_Clientes(){
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		
		String consultaSql = "SELECT dni, cliente, nombre, apellido1, apellido2, direccion, admin, contrasena" +
				" FROM clientes;";
		
		clientes = obj_conn.listar_Clientes(consultaSql);
		obj_conn.cerrarConexion();
		
		return clientes;
	}
}
	