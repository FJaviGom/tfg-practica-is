package modelo;

import java.sql.*;
import java.util.*;

import reserva_libros.Constantes;

public class Conexion {

 //private Connection conn = null;
 Connection conn; //Cargar el driver
 Statement st;  //Conectarse a la BD
 ResultSet rs;  //Procesar consultas sql
 public String mensajeError = "";

 public Conexion() {
  try {
	  Class.forName(Constantes.DB_DRIVER).newInstance();
//	  System.out.println("Driver JDBC cargado con xito");

	/*  Connection conn = DriverManager.getConnection(Constantes.DB_URL,
			  Constantes.DB_USER, Constantes.DB_PASS);	*/

  } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e ) {
   e.printStackTrace();
   mensajeError = "Error al cargar el Driver JDBC";
   System.err.println(mensajeError);
//	mensajeError = "Imposible acceder a la base de datos.";
  }
  try {
	conn = DriverManager.getConnection(Constantes.DB_URL,
			  Constantes.DB_USER, Constantes.DB_PASS);
//	System.out.println("Conexin a la Base de Datos realizada con xito");
} catch (SQLException e) {
	mensajeError = "Imposible acceder a la base de datos.";
	System.err.println(mensajeError);
	e.printStackTrace();
	}
 }

 public Connection getConexion(){
  return conn;
 }

 public void cerrarConexion(){
  try {
   conn.close();
   conn = null;
  } catch (SQLException e) {
   e.printStackTrace();
  }
 }

 public Integer insertar(String sql) throws SQLException{
	 st = conn.createStatement();
	 return st.executeUpdate(sql);
 }

 public Integer consultar(String sql) throws SQLException{
	 st = conn.createStatement();
	 rs = st.executeQuery(sql);

	 if(rs.next()) {
		 return 1;
	 }
	 else{
		 return 0;
	 }
 }

 public Integer borrar(String sql) throws SQLException{
	 st = conn.createStatement();
	 return st.executeUpdate(sql);
 }

	public ArrayList<Cliente> listar_Clientes(String sql){
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();

		try {

			st = conn.createStatement();
			rs = st.executeQuery(sql);

			while(rs.next()){
				clientes.add(new Cliente(rs.getInt("dni"), rs.getString("cliente"),rs.getString("nombre"), rs.getString("apellido1"), rs.getString("apellido2"), rs.getString("direccion"), rs.getBoolean("admin"), rs.getString("contrasena")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clientes;
	}

	public ArrayList<Libro> listar_Libros(String sql){
		ArrayList<Libro> libros = new ArrayList<Libro>();

		try {

			st = conn.createStatement();
			rs = st.executeQuery(sql);

			while(rs.next()){
				libros.add(new Libro(rs.getLong("isbn"), rs.getString("titulo"), rs.getString("autor"), rs.getString("editorial"), rs.getInt("anio_pub"), rs.getString("genero"), rs.getString("observaciones"), rs.getInt("dni")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return libros;
	}

	public ArrayList<Reserva> listar_Reservas(String sql){
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();

		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);

			while(rs.next()){
				reservas.add(new Reserva(rs.getLong("isbn"), rs.getString("titulo"), rs.getString("autor"), rs.getString("editorial"), rs.getInt("anio_pub"), rs.getString("genero"), rs.getString("observaciones"), rs.getInt("dni")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		return reservas;
		}
}
