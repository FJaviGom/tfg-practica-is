<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  import="java.sql.*, reserva_libros.Constantes, modelo.Cliente" %>

<%-- Comprobacion de login correcto. Insertamos login.jsp --%>
<%@include file="login.jsp" %>
<% 
if (request.isRequestedSessionIdValid() && !cliente.getAdmin())  { 
	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Debe ser administrador.");
} 
%>
	
<!DOCTYPE html>
<html>

  <head>
    <title>Nuevo cliente</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/estilorp.css" />
    <!--
    <script src="js/global.js"></script>
    <script src="js/funciones.js"></script>
    <script src="js/compr_nuevo_cliente.js"></script>
    -->
  </head>

  <body> 
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniería Telemática" />
				</a>
		</div>
	</div>



	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
	  <h1>Insertar nuevo cliente</h1>
	    <div id="crear">
		<div id="formcrear">
			<form action="insercion_cliente" method="post">
			<div class="dni-div">
				<label for="dni"><strong>DNI</strong></label>
				<input id="dni" type="text" value="" name="dni" maxlength="100"></input>
			</div>			
			<div class="cliente-div">
				<label for="cliente"><strong>Cliente</strong></label>
				<input id="cliente" type="text" value="" name="cliente" maxlength="100"></input>
			</div>
			<div class="nombre-div">
				<label for="nombre"><strong>Nombre</strong></label>
				<input id="nombre" type="text" value="" name="nombre" maxlength="100"></input>
			</div>
			<div class="apellido1-div">
				<label for="apellido1"><strong>Primer apellido</strong></label>
				<input id="apellido1" type="text" value="" name="apellido1" maxlength="100"></input>
			</div>
			<div class="apellido2-div">
				<label for="apellido2"><strong>Segundo apellido</strong></label>
				<input id="apellido2" type="text" value="" name="apellido2" maxlength="100"></input>
			</div>
			<div class="direccion-div">
				<label for="direccion"><strong>Dirección</strong></label>
				<input id="direccion" type="text" value="" name="direccion" maxlength="100"></input>
			</div>
			<div class="admin-div">
				<label for="admin"><strong>Administrador</strong></label>
				<input id="admin" type="checkbox" value="" name="admin"></input>
			</div>
			<div class="contrasena-div">
				<label for="contrasena"><strong>Contraseña</strong></label>
				<input id="contrasena" type="password" value="" name="contrasena" maxlength="100"></input>
			</div>
			<input class="boton" id="insertar" type="submit" value="Insertar" name="insertar"></input>
			<input class="boton" id="limpiar" type="reset" value="Limpiar datos" name="limpiar"></input>
			</form>
			
		</div>
	  </div>
	</div>
		
  </body>
</html>
