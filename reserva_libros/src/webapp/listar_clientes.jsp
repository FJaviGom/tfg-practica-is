<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/estilorp.css" />
<title>Listado de clientes</title>
</head>
<body>
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniera Telemtica" />
				</a>
		</div>
	</div>

	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
		<h1>Listado de clientes</h1>
		<div id="lista-div">
		
			<table id="lista-tabla">
				<thead>
					<tr>
						<td>DNI</td>
						<td>Cliente</td>
						<td>Nombre</td>
						<td>Primer apellido</td>
						<td>Segundo apellido</td>
						<td>Direccin</td>
						<td>Administrador</td>
						<td>Contrasea</td>
					</tr>
				</thead>
				<c:forEach var="listado_clientes" items="${sessionScope.clientes}">
					<tr>
						<td>${listado_clientes.getDni()}</td>
						<td>${listado_clientes.getCliente()}</td>
						<td>${listado_clientes.getNombre()}</td>
						<td>${listado_clientes.getApellido1()}</td>
						<td>${listado_clientes.getApellido2()}</td>
						<td>${listado_clientes.getDireccion()}</td>
						<td>${listado_clientes.getAdmin()}</td>
						<td>${listado_clientes.getContrasena()}</td>	
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>