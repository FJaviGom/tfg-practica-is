package controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.logging.Level;
import java.util.logging.Logger;

import modelo.Libro;

/**
 * Servlet implementation class Insert_libro
 */
@WebServlet(description = "Servlet controlador encargado de gestionar la insercion de un libro", urlPatterns = { "/insercion_libro" })
public class Insert_libro extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insert_libro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long isbn = null;
		String titulo = null;
		String autor = null;
		String editorial = null;
		Integer anio_pub = null;
		String genero = null;
		String observaciones = null;

		boolean control = true;

		boolean b_isbn = true;
		boolean b_titulo = true;
		boolean b_autor = true;
		boolean b_editorial = true;
		boolean b_anio_pub = true;
		boolean b_genero = true;

		// Comprobacion de que el campo isbn no sea vaco
		if(request.getParameter("isbn") == null || request.getParameter("isbn")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo isbn no puede estar vacio");

			b_isbn = false;
		}
		else {
			// Comprobacion de que isbn slo est formado por caracteres numricos
		    try {
		    	isbn = Long.parseLong(request.getParameter("isbn"));
		    } catch (Exception e) {
				Logger.getLogger(getClass().getName()).log(
		                Level.WARNING, "ERROR EN EL SERVIDOR: El campo isbn slo puede estar formado por caracteres numricos");

				b_isbn = false;
		    }
		}

        if(b_isbn == true) {
       	// Convertimos el numero a un String y comparamos (mediante el metodo length()) el
       	// numero de digitos que debe tener dicho numero)
       	String isbn_string = Long.toString(isbn);

       	if (isbn_string.length() != 10){
       		Logger.getLogger(getClass().getName()).log(
                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo isbn tiene que tener 10 caracteres");

               control = false;
//              request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);
       		}
        }


		// Comprobacion de que el campo titulo no sea vaco
		if(request.getParameter("titulo") == null || request.getParameter("titulo")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo titulo no puede estar vacio");

			b_titulo = false;
		}
		else {
			titulo = request.getParameter("titulo");
		}
		// Comprobacion de que el campo titulo tenga menos de 50 caracteres
		if(b_titulo == true){
			if(titulo.length() > 50){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo titulo tiene que tener menos de 50 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);
			}
		}

		// Comprobacion de que el campo autor no sea vaco
		if(request.getParameter("autor") == null || request.getParameter("autor")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo autor no puede estar vacio");

			b_autor = false;
		}
		else {
			autor = request.getParameter("autor");
		}
		// Comprobacion de que el campo autor tenga menos de 50 caracteres
		if(b_autor == true){
			if(autor.length() > 50){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo autor tiene que tener menos de 50 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);
			}
		}

		// Comprobacion de que el campo editorial no sea vaco
		if(request.getParameter("editorial") == null || request.getParameter("editorial")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo editorial no puede estar vacio");

			b_editorial = false;
		}
		else {
			editorial = request.getParameter("editorial");
		}
		// Comprobacion de que el campo editorial tenga menos de 50 caracteres
		if(b_editorial == true){
			if(editorial.length() > 50){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo editorial tiene que tener menos de 50 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);
			}
		}

		// Comprobacion de que el campo anio_pub no sea vaco
		if(request.getParameter("anio_pub") == null || request.getParameter("anio_pub")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo anio_pub no puede estar vacio");

			b_anio_pub = false;
		}
		else {
			// Comprobacion de que anio_pub slo est formado por caracteres numricos
		    try {
				anio_pub = Integer.parseInt(request.getParameter("anio_pub"));
		    } catch (Exception e) {
				Logger.getLogger(getClass().getName()).log(
		                Level.WARNING, "ERROR EN EL SERVIDOR: El campo anio_pub slo puede estar formado por caracteres numricos");

				b_anio_pub = false;
		    }
		}

	    if(b_anio_pub == true) {
	    // Convertimos el numero a un String y comparamos (mediante el metodo length()) el
       	// numero de digitos que debe tener dicho numero
	    String anio_pub_string = Long.toString(anio_pub);

	    if (anio_pub_string.length() > 4){
	    	Logger.getLogger(getClass().getName()).log(
	    			Level.WARNING, "ERROR EN EL SERVIDOR: El campo anio_pub no puede tener ms de 4 caracteres");

	        control = false;
//      		request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);
      		}
	    }

		// Comprobacion de que el campo genero no sea vaco (de que se haya seleccionado un gnero)
		if(request.getParameter("genero") == null || request.getParameter("genero") == "" || request.getParameter("genero").equals("0"))/*Integer.valueOf(request.getParameter("genero")) == 0*/{
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo genero no puede estar vacio. Es obligatorio elegir un gnero de la lista.");

			b_genero = false;
		}
		else {
			genero = request.getParameter("genero");
		}

	//	Long isbn = Long.parseLong(request.getParameter("isbn"));
	//	String titulo = request.getParameter("titulo");
	//	String autor = request.getParameter("autor");
	//	String editorial = request.getParameter("editorial");
	//	Integer anio_pub = Integer.parseInt(request.getParameter("anio_pub"));
	//	String genero = request.getParameter("genero");
	//	String observaciones = request.getParameter("observaciones");
	//	boolean control = true;

		observaciones = request.getParameter("observaciones");


        if (control==true){
        	Libro libro_insert = new Libro(isbn, titulo, autor, editorial, anio_pub, genero,
				observaciones);

        	if (libro_insert.insertar_Libro() > 0){
			request.getRequestDispatcher("ins_libro_correcto.jsp").forward(request, response);
        	}
        	else {
        		request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);
        	}
        }
        else {
    		request.getRequestDispatcher("ins_libro_error.jsp").forward(request, response);

        }
	}
}
