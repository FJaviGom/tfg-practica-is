/**
 * 
 */

function compruebaVacio(param,texto) {
	var result = true;
	if (param == null || param == "" || param == '') {
		alert('[ERROR] El campo ' + texto + ' no puede estar vacio.');
		result = false;
	}
return result;
}

function compruebaLongitud(param,min_long,max_long,texto) {
	var result = true;
	if (param.length < min_long || param.length > max_long)
	{
		if(min_long == max_long){
			//Caso especial DNI (tiene que cumplir el tamaño de 8 caracteres(DNI sin letra))
			alert('[ERROR] El campo ' + texto + ' tiene que tener '+ min_long + ' caracteres.');
		}
		else{
			// Resto de casos
			alert('[ERROR] El campo ' + texto + ' tiene que estar entre '+ min_long + ' y ' + max_long + ' caracteres.');
		}
	result = false;
	}
return result;
}

function compruebaMax(param,max_long,texto) {
	var result = true;
	if (param.length > max_long)
		{
			alert('[ERROR] El campo ' + texto + ' no puede tener mas de '+ max_long + ' caracteres.');
	
			result = false;
		}
	return result;
}

function compruebaISBN(param,tamanio,texto) {
	var result = true;
	if( !(/^[0-9]\d{9}$/.test(param))){
	//if (isNaN(param)==true) {//|| (/^[1-9]\d$/.test(param)==false) ) {
		alert('[ERROR] El campo ' + texto + ' tiene que tener '+ tamanio + ' caracteres numericos.');
		
		result = false;
	}
	return result;	
}	

function compruebaAnioPub(param,tamanio,texto) {
	var result = true;
	if( !(/^[0-9]\d{3}$/.test(param))){
	//if (isNaN(param)==true) {//|| (/^[1-9]\d$/.test(param)==false) ) {
		alert('[ERROR] El campo ' + texto + ' tiene que tener '+ tamanio + ' caracteres numericos.');
		
		result = false;
	}
	return result;	
}	

/*function compruebaAnioCorrecto(param){
	var result = true;
	var fecha_actual = new Date();
	var anio_actual = fecha.getFullYear();
	
	if (param > anio_actual){
		alert('[ERROR] El año de publicacion introducido es incorrecto. Por favor, introduzca un año correcto.');

		result = false;
	}
	return result;
}	*/
/*function compruebaEsNumerico(param,tamanio,texto) {
	var result = true;
//	if(!(/^[0-9]\d{9}$/.test(param))){
	if (isNaN(param)==true) {
		//|| (param.length != tamanio)) {|| (/^[1-9]\d$/.test(param)==false) ) {
		alert('[ERROR1] El campo ' + texto + ' tiene que tener '+ tamanio + ' caracteres numéricos.');
			
		result = false;
	}
	else {
		if(param.length != 10){
			alert('[ERROR2] El campo ' + texto + ' tiene que tener '+ tamanio + ' caracteres numéricos.');
		result = false;
		}
	}
	return result; 
}*/	
