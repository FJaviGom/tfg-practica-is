package unidad;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

import modelo.Libro;
import modelo.Conexion;

public class LibroTest {

	@Test
	public final void testConstructor1LibroOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 1 funciona bien");

		Libro libro = new Libro();
	}

	@Test
	public final void testConstructor2LibroOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 2 funciona bien");

		Long isbn = 123456L;

		Libro libro;

    libro = new Libro(isbn);

		assertEquals("El ISBN debería haberse asignado correctamente", libro.getIsbn(), isbn);
	}

	@Test
	public final void testConstructor3LibroOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 3 funciona bien");

		Long isbn = 123456L;
		String titulo = "Don Quijote de la Mancha";
		String autor = "Miguel de Cervantes Saavedra";
		String editorial = "Francisco de Robles";
		Integer anio_pub = 1605;
		String genero = "Novela de aventuras";
		String observaciones = null;

		Libro libro;

    libro = new Libro(isbn, titulo, autor, editorial, anio_pub, genero, observaciones);

		assertEquals("El ISBN debería haberse asignado correctamente", libro.getIsbn(), isbn);
		assertEquals("El titulo debería haberse asignado correctamente", libro.getTitulo(), titulo);
		assertEquals("El autor debería haberse asignado correctamente", libro.getAutor(), autor);
		assertEquals("La editorial debería haberse asignado correctamente", libro.getEditorial(), editorial);
		assertEquals("El anio de publicacion debería haberse asignado correctamente", libro.getAnio_pub(), anio_pub);
		assertEquals("El genero debería haberse asignado correctamente", libro.getGenero(), genero);
		assertEquals("Las observaciones deben haberse asignado correctamente", libro.getObservaciones(), observaciones);
	}

	@Test
	public final void testConstructor4LibroOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 4 funciona bien");

		Long isbn = 123456L;
		String titulo = "Don Quijote de la Mancha";
		String autor = "Miguel de Cervantes Saavedra";
		String editorial = "Francisco de Robles";
		Integer anio_pub = 1605;
		String genero = "Novela de aventuras";
		String observaciones = null;
		Integer dni = 111111;

		Libro libro;

    libro = new Libro(isbn, titulo, autor, editorial, anio_pub, genero, observaciones, dni);

		assertEquals("El ISBN debería haberse asignado correctamente", libro.getIsbn(), isbn);
		assertEquals("El titulo debería haberse asignado correctamente", libro.getTitulo(), titulo);
		assertEquals("El autor debería haberse asignado correctamente", libro.getAutor(), autor);
		assertEquals("La editorial debería haberse asignado correctamente", libro.getEditorial(), editorial);
		assertEquals("El anio de publicacion debería haberse asignado correctamente", libro.getAnio_pub(), anio_pub);
		assertEquals("El genero debería haberse asignado correctamente", libro.getGenero(), genero);
		assertEquals("Las observaciones deben haberse asignado correctamente", libro.getObservaciones(), observaciones);
		assertEquals("El dni debe haberse asignado correctamente", libro.getDni(), dni);
	}
}
