<%@ page language="java" pageEncoding="UTF-8" %>
        
<%-- Inicializa el bean "usuario" con los datos pasados 
	del formulario: usuario y clave --%>
<jsp:useBean id="cliente" class="modelo.Cliente" scope="session" />
<jsp:setProperty name="cliente" property="*" /> 

<%-- Si es incorrecto... --%>
<%
	if (!cliente.getCorrecto()) {
		//Establecemos un atributo con ambito request para indicar que
		// hay un error
		request.setAttribute("error", true);
		
		//Cerrar sesión
		session.invalidate();
		
		//reenviamos
		pageContext.forward("inicio.jsp");
	}
%>
