<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/estilorp.css" />
<title>Listado de libros</title>
</head>
<body>
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniera Telemtica" />
				</a>
		</div>
	</div>

	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
		<h1>Listado de libros</h1>
		<form action="reservar_libro" method="post">
		<div id="lista-div">
		
			<table id="lista-tabla">
				<thead>
					<tr>
						<th>ISBN</th>
						<th>Ttulo</th>
						<th>Autor</th>
						<th>Editorial</th>
						<th>Ao de publicacin</th>
						<th>Gnero</th>
						<th>Observaciones</th>
					</tr>
				</thead>
				<c:set var = "num_libros" scope = "session" value = "${sessionScope.num_libros}"/>
				<c:set var = "bandera" scope = "session" value = "${0}"/>	
				<c:forEach var="listado_libros" items="${sessionScope.libros}">
					<tr>
						<td>${listado_libros.getIsbn()}</td>
						<td>${listado_libros.getTitulo()}</td>	
						<td>${listado_libros.getAutor()}</td>
						<td>${listado_libros.getEditorial()}</td>
						<td>${listado_libros.getAnio_pub()}</td>
						<td>${listado_libros.getGenero()}</td>
						<td>${listado_libros.getObservaciones()}</td>
						<td>
							<!-- 
							Comprobacion de que el usuario es administrador 
							Si es administrador no aparece la opcin de Reservar libros (el administrador no tiene permitido reservar libros).
							En caso contrario s
							-->
							<c:if test="${cliente.getAdmin() == false}">
					
								<c:if test = "${bandera < num_libros}">
									  <c:choose>
										<c:when test="${listado_libros.getDni() != 0}">
											<c:set var="bandera" value="${bandera+1}"/>
								      		Reservado
								      	</c:when>							
								      	<c:otherwise>
								      		<c:set var="bandera" value="${bandera+1}"/>
								      		<a href="reservar_libro?isbn=${listado_libros.getIsbn()}">Reservar</a>
								      	</c:otherwise>
								    </c:choose>   
								</c:if>
							</c:if>
						</td>						
					</tr>
				</c:forEach>
			</table>
		</div>
	</form>
	</div>	
</body>
</html>