package controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Libro;
import modelo.Reserva;

/**
 * Servlet implementation class Listar_Reservas
 */
//@WebServlet(description = "Servlet controlador encargado de gestionar el listado de las reservas de todos los clientes", urlPatterns = { "/listado_reservas" })
public class Listar_Reservas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Listar_Reservas() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Reserva reserva = new Reserva();

		ArrayList<Reserva> reservas = reserva.listar_Reservas();

		request.getSession().setAttribute("reservas", reservas);		
		
		request.getRequestDispatcher("reservas.jsp").forward(request, response);

	}

}
