package unidad;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

import modelo.Reserva;
import modelo.Conexion;

public class ReservaTest {

	@Test
	public final void testConstructor1ReservaOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 1 del modelo Reserva funciona bien");

		Reserva reserva = new Reserva();
	}

	@Test
	public final void testConstructor2ReservaOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 2 del modelo Reserva funciona bien");

		Long isbn = 123456L;
		Reserva reserva;

    reserva = new Reserva(isbn);

		assertEquals("El ISBN debería haberse asignado correctamente", reserva.getIsbn(), isbn);
	}

	@Test
	public final void testConstructor3ReservaOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 3 del modelo Reserva funciona bien");

		Long isbn = 123456L;
    Integer dni = 111111;


		Reserva reserva;

    reserva = new Reserva(isbn, dni);

		assertEquals("El ISBN debería haberse asignado correctamente", reserva.getIsbn(), isbn);
    assertEquals("El dni debe haberse asignado correctamente", reserva.getDni(), dni);
	}

	@Test
	public final void testConstructor4ReservaOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 4 del modelo Reserva funciona bien");

    Long isbn = 123456L;
    String titulo = "Don Quijote de la Mancha";
		String autor = "Miguel de Cervantes Saavedra";
		String editorial = "Francisco de Robles";
		Integer anio_pub = 1605;
		String genero = "Novela de aventuras";
		String observaciones = null;
		Integer dni = 111111;

		Reserva reserva;

    reserva = new Reserva(isbn, titulo, autor, editorial, anio_pub, genero, observaciones, dni);

		assertEquals("El ISBN debería haberse asignado correctamente", reserva.getIsbn(), isbn);
		assertEquals("El titulo debería haberse asignado correctamente", reserva.getTitulo(), titulo);
		assertEquals("El autor debería haberse asignado correctamente", reserva.getAutor(), autor);
		assertEquals("La editorial debería haberse asignado correctamente", reserva.getEditorial(), editorial);
		assertEquals("El anio de publicacion debería haberse asignado correctamente", reserva.getAnio_pub(), anio_pub);
		assertEquals("El genero debería haberse asignado correctamente", reserva.getGenero(), genero);
		assertEquals("Las observaciones deben haberse asignado correctamente", reserva.getObservaciones(), observaciones);
		assertEquals("El dni debe haberse asignado correctamente", reserva.getDni(), dni);
	}
}
