<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*, reserva_libros.Constantes, modelo.Cliente"%>

<%-- Comprobacion de login correcto. Insertamos login.jsp --%>
<%@include file="login.jsp" %>

<!DOCTYPE html>
<html>

  <head>
    <title>Mis reservas</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/estilorp.css" />
    <script src="js/global.js"></script>
    <script src="js/funcionesrp.js"></script>
  </head>

  <body> 
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniería Telemática" />
				</a>
		</div>
	</div>



	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
	  <h1>Mis reservas</h1>
	    <div id="lista-div">
	  	
		<table id="lista-tabla">
			<thead>
				<tr>
					<th>ISBN</th>
					<th>Título</th>
					<th>Autor</th>
					<th>Editorial</th>
					<th>Año de publicación</th>
					<th>Género</th>
					<th>Observaciones</th>
				</tr>
			</thead>
			<%
			//Mensaje de error, si se produce algún error o excepción al acceder a la 
			// base de datos.
			String mensajeError = "";
			//Consulta SQL
		    String sql="SELECT libros.* FROM libros, reservas" +
		    		" WHERE libros.isbn = reservas.isbn AND reservas.dni = "+ cliente.getDni() + ";";
			//constantes
			//Nos conectamos a la base de datos
			try {
				Class.forName(Constantes.DB_DRIVER).newInstance();
				
				Connection conn = DriverManager.getConnection(Constantes.DB_URL,
						Constantes.DB_USER, Constantes.DB_PASS);
				try {
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					while (rs.next()) {
						//Generamos tabla
						long isbn = rs.getLong(1);
						String titulo = rs.getString(2);						
						String autor = rs.getString(3);
						String editorial = rs.getString(4);
						int anio_pub = rs.getInt(5);
						String genero = rs.getString(6);
						String observaciones = rs.getString(7);
						%>
						<tr id='libro-<%=isbn%>'>
							<td><%=isbn%></td>
							<td><%=titulo%></td>
							<td><%=autor%></td>
							<td><%=editorial%></td>
							<td><%=anio_pub%></td>
							<td><%=genero%></td>
							<td><%=observaciones%></td>
							<td>
				<!--  				<button value="Borrar" onClick="location='borrar_reserva'">Borrar</button>
-->
							</td>
						</tr>
						<%
					}
					rs.close();
					st.close();
				} catch (SQLException e) {
					mensajeError = "No se ha podido obtener el listado de libros." + e;
				}
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				mensajeError = "Imposible acceder a la base de datos.";
			}
		%>
		</table>
		<br><br><hr>
		
	</div>
	<%
	//Muestra mensaje de error
	if ( !mensajeError.isEmpty() ) {
	%>
		<div id="error"><p> ERROR: 
		<%=mensajeError%>
		</p></div>
	  <%
	}
	%>
	</div>

  </body>
</html>