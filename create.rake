require 'active_record/fixtures'

N_PAREJAS = 10

namespace :dit do
	desc "DIT | Inicializar los datos"
	task :inicializar => :environment do
		puts "Inicializando datos"

		Rake::Task['gitlab:db:drop_tables'].invoke
		Rake::Task['gitlab:db:configure'].invoke

		admin = User.create!(name: "Dit", email: "admin@dit.com", password:"D!t12345", username: "dit", admin: true)
		admin.confirm

		Rake::Task['dit:inicializar_grupo'].invoke(1)
		Rake::Task['dit:inicializar_grupo'].reenable
		Rake::Task['dit:inicializar_grupo'].invoke(2)
		Rake::Task['dit:inicializar_grupo'].reenable
		Rake::Task['dit:inicializar_grupo'].invoke(3)

		ApplicationSetting.update(signup_enabled: false, default_branch_protection: 0)
		Ci::Runner.create!(token: "dit", runner_type: "instance_type", first_day_of_week: 1)
	end

	desc "DIT | Inicializar datos grupo"
	task :inicializar_grupo, [:n_grupo] => :environment do |_, args|
		n_grupo = args[:n_grupo]

    unless n_grupo
      puts "Must specify a migration version as an argument".color(:red)
      exit 1
    end

		admin = User.find_by(username: "dit")

		puts "No se ha ejecutado la tarea inicial." && return unless admin.present?

		puts "Inicializando Grupo #{n_grupo}"

		repositorio = Project.find_by(name:"ProyectoG#{n_grupo}")
		if repositorio.present?
			puts("El repositorio \"ProyectoG#{n_grupo}\" está siendo reiniciado")

			Projects::DestroyService.new(repositorio, admin).execute
		end

		repositorio = Projects::CreateService.new(admin, name: "ProyectoG#{n_grupo}", path: "proyecto#{n_grupo}", skip_disk_validation: true).execute
		repositorio.add_maintainer(admin)

		destroy_users = Users::DestroyService.new(admin)

		for i in 1..N_PAREJAS  do
			# Creando Desarollador
			dev = User.find_by(username: "dev#{n_grupo}-#{i}")

			if dev.present?
				puts("Usuario dev#{n_grupo}-#{i} reiniciado")

				destroy_users.execute(dev)
			end

			dev = Users::CreateService.new(
				admin,
				name: "Developer#{n_grupo}-#{i}",
				email: "dev#{n_grupo}-#{i}@dit.com",
				password:"developer#{n_grupo}-#{i}",
				username: "dev#{n_grupo}-#{i}"
			).execute
			dev.confirm

			repositorio.add_user(dev,30)

			# Creando Lider de equipo

			lider = User.find_by(username: "lider#{n_grupo}-#{i}")

			if lider.present?
				puts("Usuario lider#{n_grupo}-#{i} reiniciado")

				destroy_users.execute(lider)
			end

			lider = Users::CreateService.new(
				admin,
				name: "Lider#{n_grupo}-#{i}",
				email: "lider#{n_grupo}-#{i}@dit.com",
				password:"lider#{n_grupo}-#{i}",
				username: "lider#{n_grupo}-#{i}"
			).execute
			lider.confirm

			repositorio.add_user(lider,30)
		end
	end
end # namespace end: dit
