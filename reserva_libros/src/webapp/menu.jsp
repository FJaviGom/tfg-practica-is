<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
  
<%-- Crea el menu --%>

<%-- Solo ADMINISTRADORES --%>
<jsp:useBean id="cliente" class="modelo.Cliente" scope="session" />
<% if (cliente.getAdmin()) { %>
	<p class='boton'><a href="nuevo_libro.jsp">Insertar libro</a></p>
	<p class='boton'><a href="nuevo_cliente.jsp">Insertar cliente</a></p>
	<p class='boton'><a href="listado_reservas">Reservas</a></p>	
	<p class='boton'><a href="listar_clientes">Listado de clientes</a></p> <%-- Llama primero al controlador para que se cargue la lista a mostrar antes de mostrarla --%>
<% } %>

<%-- Solo CLIENTES --%>
<% if (!cliente.getAdmin()) { %>
	<p class='boton'><a href="listado_reservas_cliente">Mis reservas</a></p>
<% } %>

<%-- TODOS --%>
<p class='boton'><a href="listado_libros">Listado de libros</a></p>
<p class='boton'><a href="datos.jsp">Datos</a></p>
<p class='boton'><a href="logout.jsp">Salir</a></p>
