package unidad;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

import modelo.Cliente;
import modelo.Conexion;

public class ClienteTest {

	@Test
	public final void testConstructor1ClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 1 del modelo Cliente funciona bien");

		Cliente cliente = new Cliente();
	}

	@Test
	public final void testConstructor2ClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 2 del modelo Cliente funciona bien");

		Integer dni = 111111;

		Cliente cliente;

    cliente = new Cliente(dni);

		assertEquals("El dni debería haberse asignado correctamente", cliente.getDni(), dni);
	}

	@Test
	public final void testConstructor3ClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 3 del modelo Cliente funciona bien");

		Integer dni = 111111;
		String usuario = "Usuario1";
		String nombre = "Nombre";
		String apellido1 = "Primer apellido";
		String apellido2 = "Segundo apellido";
		String direccion = "Dirección del cliente";
		Boolean admin = true;
		String contrasena = "contraseña";

		Cliente cliente;

    cliente = new Cliente(dni, usuario, nombre, apellido1, apellido2, direccion, admin, contrasena);

		assertEquals("El dni debería haberse asignado correctamente", cliente.getDni(), dni);
		assertEquals("El cliente debería haberse asignado correctamente", cliente.getCliente(), usuario);
		assertEquals("El nombre debería haberse asignado correctamente", cliente.getNombre(), nombre);
		assertEquals("El apellido1 debería haberse asignado correctamente", cliente.getApellido1(), apellido1);
		assertEquals("El apellido2 debería haberse asignado correctamente", cliente.getApellido2(), apellido2);
		assertEquals("La direccion debería haberse asignado correctamente", cliente.getDireccion(), direccion);
		assertEquals("El valor admin debería haberse asignado correctamente", cliente.getAdmin(), admin);
		assertEquals("La contrasena debería haberse asignado correctamente", cliente.getContrasena(), contrasena);
	}

	@Test
	public final void testConstructor4ClienteOK() {
		System.out.println("Comienza la prueba que comprueba que el constructor 4 del modelo Cliente funciona bien");

		Integer dni = 111111;
		String nombre = "Nombre";
		String apellido1 = "Primer apellido";
		String apellido2 = "Segundo apellido";
		String direccion = "Dirección del cliente";
		Boolean admin = true;

		Cliente cliente;

    cliente = new Cliente(dni, nombre, apellido1, apellido2, direccion, admin);

		assertEquals("El dni debería haberse asignado correctamente", cliente.getDni(), dni);
		assertEquals("El nombre debería haberse asignado correctamente", cliente.getNombre(), nombre);
		assertEquals("El apellido1 debería haberse asignado correctamente", cliente.getApellido1(), apellido1);
		assertEquals("El apellido2 debería haberse asignado correctamente", cliente.getApellido2(), apellido2);
		assertEquals("La direccion debería haberse asignado correctamente", cliente.getDireccion(), direccion);
		assertEquals("El valor admin debería haberse asignado correctamente", cliente.getAdmin(), admin);
	}
}
