<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" type="text/css" href="css/estilorp.css" />
    <title>Reservas</title>
  </head>

  <body> 
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniera Telemtica" />
				</a>
		</div>
	</div>

	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
	  <h1>Reservas</h1>
	  <form action="borrar_reserva" method="post">
	    <div id="lista-div">
	  	
				<table id="lista-tabla">
					<thead>
						<tr>
							<th>ISBN</th>
							<th>Ttulo</th>
							<th>Autor</th>
							<th>Editorial</th>
							<th>Ao de publicacin</th>
							<th>Gnero</th>
							<th>Observaciones</th>
							<th>DNI_cliente</th>
						</tr>
					</thead>
					<c:forEach var="listado_reservas" items="${sessionScope.reservas}">
						<tr>
							<td>${listado_reservas.getIsbn()}</td>
							<td>${listado_reservas.getTitulo()}</td>							
							<td>${listado_reservas.getAutor()}</td>
							<td>${listado_reservas.getEditorial()}</td>
							<td>${listado_reservas.getAnio_pub()}</td>
							<td>${listado_reservas.getGenero()}</td>
							<td>${listado_reservas.getObservaciones()}</td>
							<td>${listado_reservas.getDni()}</td>
							<td>
								<a href="borrar_reserva?isbn=${listado_reservas.getIsbn()}" >Borrar</a>
							</td>						
						</tr>
					</c:forEach>
				</table>
			</div>
		</form>
		</div>
	</body>
</html>	