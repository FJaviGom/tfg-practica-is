<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*, java.util.Vector,java.util.ArrayList, modelo.Cliente, reserva_libros.Constantes"%>

<%-- Comprobacion de login correcto. Insertamos login.jsp --%>
<%@include file="login.jsp" %>
<% 
if (request.isRequestedSessionIdValid() && !cliente.getAdmin())  { 
	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Debe ser administrador.");
} 
%>
	
<!DOCTYPE html>
<html>

  <head>
    <title>Nuevo libro</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/estilorp.css" />
    <!--  
    <script type="text/javascript" src="js/global.js"></script>
    <script type="text/javascript" src="js/compr_nuevo_libro.js"></script> 
 	<script type="text/javascript" src="js/funciones.js"></script>    
 	-->   	
  </head>

  <body> 
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniería Telemática" />
				</a>
		</div>
	</div>

	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
	  <h1>Insertar nuevo libro</h1>
	    <div id="crear">
		<div id="formcrear">
			<form method="post" action="insercion_libro" onsubmit="return compr_nuevo_libro();">
			<div class="isbn-div">
				<label for="isbn"><strong>ISBN</strong></label>
				<input id="isbn" type="text" value="" name="isbn" maxlength="100"></input>
			</div>
			<div class="titulo-div">
				<label for="titulo"><strong>Título</strong></label>
				<input id="titulo" type="text" value="" name="titulo" maxlength="100"></input>
			</div>
			<div class="autor-div">
				<label for="autor"><strong>Autor</strong></label>
				<input id="autor" type="text" value="" name="autor" maxlength="100"></input>
			</div>
			<div class="editorial-div">
				<label for="editorial"><strong>Editorial</strong></label>
				<input id="editorial" type="text" value="" name="editorial" maxlength="100"></input>
			</div>
			<div class="anio_pub-div">
				<label for="anio_pub"><strong>Año de publicación</strong></label>
				<input id="anio_pub" type="text" value="" name="anio_pub" maxlength="100"></input>
			</div>
			<div class="genero-div">
				<label for="genero"><strong>Género</strong></label>
				<select id="genero" name="genero">
				<% 
			//Mensaje de error, si se produce algún error o excepción al acceder a la 
			// base de datos.
			String mensajeError = "";
			//Consulta SQL
		    String sql="SELECT genero FROM generos;";
			//constantes
			
     //       String combobox = request.getParameter("combobox"); 
			//Nos conectamos a la base de datos
			try {
				Class.forName(Constantes.DB_DRIVER).newInstance();
				
				Connection conn = DriverManager.getConnection(Constantes.DB_URL,
						Constantes.DB_USER, Constantes.DB_PASS);
				try {
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					ArrayList vec=new ArrayList(); 
			//		Vector vec=new Vector(); 
			//		combobox.removeAllItems();
					while (rs.next()) {			
						vec.add(rs.getString(1)); 
				//		combobox.addItem(rs.getString(1));
					}	%>
					
					<option value="0" selected>Seleccione</option> 
					<%for(int i=0;i<vec.size();i++){%> 
					<option value="<%=vec.get(i).toString().trim()%>"><%=vec.get(i).toString().trim()%></option> 
					<% } 
					rs.close();
					st.close();
				} catch (SQLException e) {
					mensajeError = "No se ha podido obtener el listado de libros." + e;
				}
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				mensajeError = "Imposible acceder a la base de datos.";
			}   %>
		
		
				</select>
			</div>
			<div class="observaciones-div">
				<label for="observaciones"><strong>Observaciones</strong></label>
				<input id="observaciones" type="text" value="" name="observaciones" maxlength="100"></input>
			</div>
			<input class="boton" id="insertar" type="submit" value="Insertar" name="insertar"></input>
			<input class="boton" id="limpiar" type="reset" value="Limpiar datos" name="limpiar"></input>
			</form>
			
		</div>
	  </div>
	</div>
	
  </body>
</html>
