

var nombreCookie = "estilo";

//Función para acortar las llamadas a getElementById
function elem(id) {
	return document.getElementById(id);
}


//Valida el formulario de login
function validacionLogin() {
	var cliente = document.getElementById("cliente").value;
	var contrasena = document.getElementById("contrasena").value;
	var resultado = true;
/*	var min_cont = 5;
	var max_cont = 12;
	var min_client = 5;
	var max_client = 12;	*/

	resultado = compruebaVacio(cliente,"cliente");
	if (resultado == true){
		resultado = compruebaLongitud(cliente,min_client,max_client,"cliente");
	}

	resultado = compruebaVacio(contrasena,"contraseña");
	if (resultado == true){
		resultado = compruebaLongitud(cliente,min_client,max_client,"contraseña");
	}	

	// Si el script ha llegado a este punto, todas las condiciones
	// se han cumplido, por lo que se devuelve el valor true
	return resultado;
}


//Función que obtiene las cookies
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Crea, modifica
function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname+"="+cvalue+"; "+expires;
}

//Borra una cookie
function deleteCookie(name) {
	document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

//Comprueba si existe la cookie, para el formulario 
function checkCookie() {
    checkbox = elem("estiloAlt");
    if (checkbox != null) {
	    var estilo=getCookie(nombreCookie);
	    if (estilo != "") {
	    	checkbox.checked = true;
	    } else {
	    	checkbox.checked = false;
	    }
    }
}

window.onload = function() {
	//	Tareas a realizar al principio.
	checkCookie();
	elem("formacceso").onsubmit=validacionLogin;	
}
