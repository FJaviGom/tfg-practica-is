package modelo;

import java.sql.*;
import java.util.ArrayList;

import reserva_libros.Constantes;

public class Libro {

	private Long isbn;

	private String titulo;

	private String autor;

	private String editorial;

	private Integer anio_pub;

	private String genero;

	private String observaciones;

	//Campo usado para ver el cliente que tiene reservado un libro
	private Integer dni;

	public Conexion obj_conn;


	/**
	 * Constructor vaco
	 */
	public Libro(){
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
	}

	/**
	 * Constructor con parmetro la clave primaria (ISBN)
	 */
	public Libro(Long isbn) {
		this.isbn = isbn;

		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();

	}

	/**
	 * Constructor con todos los campos
	 */

	public Libro(Long isbn, String titulo, String autor, String editorial, Integer anio_pub, String genero,
			String observaciones) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.editorial = editorial;
		this.anio_pub = anio_pub;
		this.genero = genero;
		this.observaciones = observaciones;

		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
	}

	/**
	 * Constructor con todos los campos y el dni del Cliente que tiene reservado el Libro
	 */

	public Libro(Long isbn, String titulo, String autor, String editorial, Integer anio_pub, String genero,
			String observaciones, Integer dni) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.editorial = editorial;
		this.anio_pub = anio_pub;
		this.genero = genero;
		this.observaciones = observaciones;
		this.dni = dni;

		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
	}

	/**
	 * @return the isbn
	 */
	public Long getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the editorial
	 */
	public String getEditorial() {
		return editorial;
	}

	/**
	 * @param editorial the editorial to set
	 */
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	/**
	 * @return the anio_pub
	 */
	public Integer getAnio_pub() {
		return anio_pub;
	}

	/**
	 * @param anio_pub the anio_pub to set
	 */
	public void setAnio_pub(Integer anio_pub) {
		this.anio_pub = anio_pub;
	}

	/**
	 * @return the genero
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * @param genero the genero to set
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * Lo usamos para obtener el cliente que tiene reservado un libro
	 */
	/**
	 * @return the dni
	 */
	public Integer getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(Integer dni) {
		this.dni=dni;
	}


	/**************** Mtodos ******/

	/**
	 *
	 * @param
	 * @return
	 */

	public Integer insertar_Libro(){
		//Verificamos que estamos en una sesion, usuario administrador

//			Cliente_Bean cliente = new Cliente_Bean();
//			int resultado = -1;

//			if(cliente.getCorrecto() && cliente.getAdmin()) {

			//    int resultado = -1;
			    //Nos conectamos a la base de datos

		int resultado = 0;

		try {

		String consultaSql = "INSERT INTO libros (isbn, titulo, autor, editorial, anio_pub, genero, observaciones) VALUES ('"+
			    				isbn+"', '"+
			    				titulo+"', '"+
			    				autor+"', '"+
			    				editorial+"', '"+
			    				anio_pub+"', '"+
			    				genero+"', '"+
			    				observaciones+"');";

		resultado = obj_conn.insertar(consultaSql);
		} catch (SQLException e) {
			obj_conn.cerrarConexion();
			e.printStackTrace();

			return -1;   //Se est intentando registrar una PK duplicada
				}
		obj_conn.cerrarConexion();
		return resultado;
		/*	    try {
					Class.forName(Constantes.DB_DRIVER).newInstance();

					Connection conn = DriverManager.getConnection(Constantes.DB_URL,
							Constantes.DB_USER, Constantes.DB_PASS);
			   		Statement st = conn.createStatement();
			    	int resUpdate = st.executeUpdate( consultaSql );
			    	if (resUpdate > 0 ) {
			    		resultado = 0;
			    	} else {
			    		resultado = 1;
			    	}
					st.close();
			    	conn.close();

				} catch (Exception e) {
					resultado = 1;
					e.printStackTrace();
				}
//			    out.println(resultado);
				//}
			 return resultado;	*/
			}

	public ArrayList<Libro> listar_Libros(){
		ArrayList<Libro> libros = new ArrayList<Libro>();

/*		String consultaSql = "SELECT isbn, titulo, autor, editorial, anio_pub, genero, observaciones" +
				" FROM libros;";
*/

		String consultaSql = "SELECT l.isbn, l.titulo, l.autor, l.editorial, l.anio_pub, l.genero, l.observaciones, r.dni" +
				" FROM libros l LEFT JOIN reservas r ON l.isbn = r.isbn;";

		libros = obj_conn.listar_Libros(consultaSql);

		obj_conn.cerrarConexion();

		return libros;
	}

}
