/**
 * 
 */
package modelo;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author Fabian
 *
 */
public class Reserva {
	// Campos de la clase Reserva referentes a la clase Libro
	private Long isbn;
	
	private String titulo;
	
	private String autor;
	
	private String editorial;
	
	private Integer anio_pub;
	
	private String genero;
	
	private String observaciones;		

	// Campos de la clase Reserva referentes a la clase Cliente	
	private Integer dni;
	
	Conexion obj_conn;

	
	/**
	 * Constructor vaco
	 */
	public Reserva(){
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();		
	}
	
	/**
	 * Constructor con parmetro la clave primaria (ISBN)
	 */
	public Reserva(Long isbn) {
		this.isbn = isbn;
		
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();

	}	
	
	/**
	 * Constructor con los campos isbn y dni
	 */
	public Reserva(Long isbn, Integer dni){
		this.isbn = isbn;
		this.dni = dni;
		
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
	}
	
	public Reserva(Long isbn, String titulo, String autor, String editorial, Integer anio_pub, String genero,
			String observaciones, Integer dni) {

/*		
		Libro libro = new Libro(isbn, titulo, autor, editorial, anio_pub, genero, observaciones);
		Cliente cliente = new Cliente(dni);
		this.isbn = libro.getIsbn();
		this.titulo = libro.getTitulo();
		this.autor = libro.getAutor();
		this.editorial = libro.getEditorial();
		this.anio_pub = libro.getAnio_pub();
		this.genero = libro.getGenero();
		this.observaciones = libro.getObservaciones();
		this.dni = cliente.getDni();
*/

		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.editorial = editorial;
		this.anio_pub = anio_pub;
		this.genero = genero;
		this.observaciones = observaciones;
		this.dni=dni;

		
		//Instanciamos un objeto de la clase conexin
//		obj_conn = new Conexion();
	}
	
	/**
	 * Constructor con todos los campos
	 * @param libro
	 * @param cliente
	 */
	public Reserva(Libro libro, Cliente cliente){
		this.isbn = libro.getIsbn();
		this.titulo = libro.getTitulo();
		this.autor = libro.getAutor();
		this.editorial = libro.getEditorial();
		this.anio_pub = libro.getAnio_pub();
		this.genero = libro.getGenero();
		this.observaciones = libro.getObservaciones();
		this.dni = cliente.getDni();
		
		//Instanciamos un objeto de la clase conexin
		obj_conn = new Conexion();
		
	}
	
	
	// Setter y Getter
	/**
	 * @return the isbn
	 */
	public Long getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the editorial
	 */
	public String getEditorial() {
		return editorial;
	}

	/**
	 * @param editorial the editorial to set
	 */
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	/**
	 * @return the anio_pub
	 */
	public Integer getAnio_pub() {
		return anio_pub;
	}

	/**
	 * @param anio_pub the anio_pub to set
	 */
	public void setAnio_pub(Integer anio_pub) {
		this.anio_pub = anio_pub;
	}

	/**
	 * @return the genero
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * @param genero the genero to set
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}	
	
	/**
	 * @return the dni
	 */
	public Integer getDni() {
		return dni;
	}
	
	/**
	 * @param dni the dni to set
	 */
	public void setDni(Integer dni) {
		this.dni=dni;
	}	
		

	/**************** Mtodos ******/	

	public Integer reservar_Libro(){
		int resultado = 0;
		
		try {
		
		String consultaSql = "INSERT INTO reservas (isbn, dni) VALUES ('"+
		   		isbn+"', '"+
		   		dni+"');";
			
		resultado = obj_conn.insertar(consultaSql);
		} catch (SQLException e) {
			obj_conn.cerrarConexion();
			return -1;   //Se est intentando registrar una PK duplicada
					//e.printStackTrace();
				}
		obj_conn.cerrarConexion();
		return resultado;
			}
	
	public Integer borrar_Reserva(Long n_isbn){
		int resultado = 0;
		
		try {
		
		String consultaSql = "DELETE FROM reservas"+
				" WHERE isbn="+ n_isbn;
			
		obj_conn.borrar(consultaSql);
		resultado = 1;
		} catch (SQLException e) {
			obj_conn.cerrarConexion();
			return 0;   //Se est intentando registrar una PK duplicada
					//e.printStackTrace();
				}
		obj_conn.cerrarConexion();
		return resultado;
		}
	
	public ArrayList<Reserva> listar_Reservas(){
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		
		String consultaSql = "SELECT libros.*, reservas.dni" +
				"  FROM libros, reservas WHERE libros.isbn = reservas.isbn;";

		/*
			    String sql="SELECT libros.*,reservas.dni FROM libros, reservas" +
			    		" WHERE libros.isbn = reservas.isbn;";
		*/
		
		reservas = obj_conn.listar_Reservas(consultaSql);

		obj_conn.cerrarConexion();
		
		return reservas;
	}
	
	public ArrayList<Reserva> listar_Reservas_Cliente(Integer dni){
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		System.out.println("DNI_cliente:"+dni);
		
		String consultaSql = "SELECT libros.*, reservas.dni" +
				"  FROM libros, reservas WHERE libros.isbn = reservas.isbn AND reservas.dni = "+ dni + ";";

		/*
			    String sql="SELECT libros.*,reservas.dni FROM libros, reservas" +
			    		" WHERE libros.isbn = reservas.isbn;";
		*/
		
		reservas = obj_conn.listar_Reservas(consultaSql);

		obj_conn.cerrarConexion();
		
		return reservas;
	}

}
