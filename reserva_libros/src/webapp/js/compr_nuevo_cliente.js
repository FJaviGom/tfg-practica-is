

//Comprueba el formulario de inserción de un nuevo cliente
function compruebaNuevoCliente() {
	var dni = document.getElementById("dni").value;
	var cliente = document.getElementById("cliente").value;
	var nombre = document.getElementById("nombre").value;
	var apellido1 = document.getElementById("apellido1").value;
	var apellido2 = document.getElementById("apellido2").value;
	var direccion = document.getElementById("direccion").value;
	var admin = document.getElementById("admin").value;
	var contrasena = document.getElementById("contrasena").value;
	var resultado = true;

	resultado = compruebaVacio(dni,"dni");
	if (resultado == true){
		resultado = compruebaLongitud(dni,min_dni,max_dni,"dni");
	}
	
	resultado = compruebaVacio(cliente,"cliente");
	if (resultado == true){
		resultado = compruebaLongitud(cliente,min_client,max_client,"cliente");
	}

	resultado = compruebaVacio(nombre,"nombre");
	if (resultado == true){
		resultado = compruebaMax(nombre,max_nombre,"nombre");
	}
	
	resultado = compruebaVacio(apellido1,"primer apellido");
	if (resultado == true){
		resultado = compruebaMax(apellido1,max_apellido,"primer apellido");
	}

	resultado = compruebaVacio(apellido2,"segundo apellido");
	if (resultado == true){
		resultado = compruebaMax(apellido2,max_apellido,"segundo apellido");
	}
	
	resultado = compruebaVacio(direccion,"direccion");
	if (resultado == true){
		resultado = compruebaMax(direccion,max_direccion,"direccion");
	}

	resultado = compruebaVacio(contrasena,"contraseña");
	if (resultado == true){
		resultado = compruebaLongitud(cliente,min_client,max_client,"contraseña");
	}	

	// Si el script ha llegado a este punto, todas las condiciones
	// se han cumplido, por lo que se devuelve el valor true
	return resultado;
}
