<%@ page language="java" pageEncoding="UTF-8" 
	import="java.sql.*, reserva_libros.Constantes"
	trimDirectiveWhitespaces="true"%>
    
<jsp:useBean id="cliente" class="modelo.Cliente" scope="session" />

<%
/*
Si el usuario es correcto la respuesta será:
	0  = exito
	1  = error
En caso contrario, no devolverá nada.
*/

//Verificamos que estamos en una sesion
if(cliente.getCorrecto()) {
    	
    int resultado = -1;
    //Nos conectamos a la base de datos
    String consultaSql = "INSERT INTO reservas (isbn, dni) VALUES ('"+
    		request.getParameter("isbn")+"', '"+
    		cliente.getDni()+"');";
    try {
		Class.forName(Constantes.DB_DRIVER).newInstance();
		
		Connection conn = DriverManager.getConnection(Constantes.DB_URL,
				Constantes.DB_USER, Constantes.DB_PASS);
   		Statement st = conn.createStatement();
    	int resUpdate = st.executeUpdate( consultaSql );
    	if (resUpdate > 0 ) {
    		resultado = 0;
    	} else {
    		resultado = 1;
    	}
		st.close();
    	conn.close();
    	
	} catch (Exception e) {
		resultado = 1;
		e.printStackTrace();
	}
    out.println(resultado);
}
%>