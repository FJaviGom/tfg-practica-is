<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <head>
    <title>Reserva de libros</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/estilorp.css" />
	<script src="js/global.js"></script>    
	<script src="js/jsinicio.js"></script>
	<script src="js/funciones.js"></script>
	
  </head>

  <body> 

	<div id="bienvenida">
		<h1> Error en el usuario o contrasea. Por favor, introdzcalos correctamente.</h1>
		<h1>Bienvenido a</h1>
		<p><img src="images/logoDep.jpg" alt="Departamento de Ingeniera Telemtica" /></p>
			<h3>Acceda con su usuario y clave</h3>
		<div id="formacceso">
			<form action="login" method="post">
				<p> <label for="usuario">Cliente:</label><br/>
				<input type="text" name="cliente" size="20" id="cliente" /></p>
				<p> <label for="clave">Contrasea:</label> <br/>
				<input type="password" name="contrasena" size="20" id="contrasena" /></p>
				<p> 
				<input class="boton" type="submit" name="entrar" value="Entrar" />
				<input class="boton" type="button" name="resetear" value="Limpiar campos" onclick = "location='inicio.jsp'"/>
				</p>
			</form>
		</div>
	</div>
	
  </body>
</html>