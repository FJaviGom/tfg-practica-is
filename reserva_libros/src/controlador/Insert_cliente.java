package controlador;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Cliente;
import modelo.NuevoCliente;

/**
 * Servlet implementation class Insert_cliente
 */
@WebServlet("/insercion_cliente")
public class Insert_cliente extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insert_cliente() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer dni = null;
		String cliente = null;
		String nombre = null;
		String apellido1 = null;
		String apellido2 = null;
		String direccion = null;
		String contrasena = null;

		boolean control = true;

		boolean b_dni = true;
		boolean b_cliente = true;
		boolean b_nombre = true;
		boolean b_apellido1 = true;
		boolean b_apellido2 = true;
		boolean b_direccion = true;
		boolean b_contrasena = true;

		// Comprobacion de que el campo dni no sea vacio
		if(request.getParameter("dni") == null || request.getParameter("dni")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo dni no puede estar vacio");

			b_dni = false;
		}
		else {
						// Comprobacion de que isbn solo esta formado por caracteres numericos
		    try {
		    	dni = Integer.parseInt(request.getParameter("dni"));
		    } catch (Exception e) {
				Logger.getLogger(getClass().getName()).log(
		                Level.WARNING, "ERROR EN EL SERVIDOR: El campo isbn slo puede estar formado por caracteres numricos");

				b_dni = false;
		    }
		}

        if(b_dni == true) {
       	// Convertimos el numero a un String y comparamos (mediante el metodo length()) el
       	// numero de digitos que debe tener dicho numero)
       	String dni_string = Integer.toString(dni);

       	if (dni_string.length() != 8){
       		Logger.getLogger(getClass().getName()).log(
                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo dni tiene que tener 8 caracteres");

               control = false;
//               request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
       		}
        }

		// Comprobacion de que el campo cliente no sea vaco
		if(request.getParameter("cliente") == null || request.getParameter("cliente")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo cliente no puede estar vacio");

			b_cliente = false;
		}
		else {
			cliente = request.getParameter("cliente");
		}
		// Comprobacion de que el campo cliente tenga entre 5 y 12 caracteres
		if(b_cliente == true){
			if(cliente.length() < 5 || cliente.length() > 12){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo cliente tiene que estar entre 5 y 12 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
			}
		}


		// Comprobacion de que el campo nombre no sea vaco
		if(request.getParameter("nombre") == null || request.getParameter("nombre")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo nombre no puede estar vacio");

			b_nombre = false;
		}
		else {
			nombre = request.getParameter("nombre");
		}
		// Comprobacion de que el campo nombre tenga menos de 20 caracteres
		if(b_nombre == true){
			if(nombre.length() > 20){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo nombre tiene que tener menos de 20 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
			}
		}


		// Comprobacion de que el campo apellido1 no sea vaco
		if(request.getParameter("apellido1") == null || request.getParameter("apellido1")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo apellido1 no puede estar vacio");

			b_apellido1 = false;
		}
		else {
			apellido1 = request.getParameter("apellido1");
		}
		// Comprobacion de que el campo apellido1 tenga menos de 20 caracteres
		if(b_apellido1 == true){
			if(apellido1.length() > 20){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo apellido1 tiene que tener menos de 20 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
			}
		}


		// Comprobacion de que el campo apellido2 no sea vaco
		if(request.getParameter("apellido2") == null || request.getParameter("apellido2")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo apellido2 no puede estar vacio");

			b_apellido2 = false;
		}
		else {
			apellido2 = request.getParameter("apellido2");
		}
		// Comprobacion de que el campo apellido2 tenga menos de 20 caracteres
		if(b_apellido2 == true){
			if(apellido2.length() > 20){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo apellido2 tiene que tener menos de 20 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
			}
		}


		// Comprobacion de que el campo direccion no sea vaco
		if(request.getParameter("direccion") == null || request.getParameter("direccion")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo direccion no puede estar vacio");

			b_direccion = false;
		}
		else {
			direccion = request.getParameter("direccion");
		}
		// Comprobacion de que el campo apellido2 tenga menos de 40 caracteres
		if(b_direccion == true){
			if(direccion.length() > 40){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo direccion tiene que tener menos de 20 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
			}
		}


//		Integer dni = Integer.parseInt(request.getParameter("dni"));
//		String cliente = request.getParameter("cliente");
//		String nombre = request.getParameter("nombre");
//		String apellido1 = request.getParameter("apellido1");
//		String apellido2 = request.getParameter("apellido2");
//		String direccion = request.getParameter("direccion");
	//	boolean admin = Boolean.valueOf(request.getParameter("admin"));
		Boolean admin = Boolean.parseBoolean(request.getParameter("admin"));
		String admin_string = request.getParameter("admin");
		try{
			if(admin_string.compareTo("on")==0){
				admin=false;
			}
			else{
				admin=true;
			}
		}catch(java.lang.NullPointerException e){};

	//	Integer admin = Integer.parseInt(request.getParameter("admin"));
	//	byte admin = Byte.valueOf(request.getParameter("admin"));
	//	String admin = request.getParameter("admin");


		// Comprobacion de que el campo contrasea no sea vaco
		if(request.getParameter("contrasena") == null || request.getParameter("contrasena")==""){
			Logger.getLogger(getClass().getName()).log(
                Level.WARNING, "ERROR EN EL SERVIDOR: El campo contrasena no puede estar vacio");

			b_contrasena = false;
		}
		else {
			contrasena = request.getParameter("contrasena");
		}
		// Comprobacion de que el campo contrasena tenga entre 5 y 12 caracteres
		if(b_contrasena == true){
			if(contrasena.length() < 5 || contrasena.length() > 12){
	      		Logger.getLogger(getClass().getName()).log(
	                       Level.WARNING, "ERROR EN EL SERVIDOR: El campo contrasena tiene que estar entre 5 y 12 caracteres");

	      		control = false;
//	       		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
			}
		}


//		String contrasena = request.getParameter("contrasena");

//		byte admin_byte = (byte)(admin?1:0);


	    if (control==true){
	    	Cliente cliente_insert = new Cliente(dni, cliente, nombre, apellido1, apellido2,
	    			direccion, admin, contrasena);

	    	if (cliente_insert.insertar_Cliente() > 0){
	    		request.getRequestDispatcher("ins_cliente_correcto.jsp").forward(request, response);
	    	}
	    	else {
	    		request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
	    	}
	    }
	    else { //if(control==false)
	    	request.getRequestDispatcher("ins_cliente_error.jsp").forward(request, response);
	    }
	}
}
