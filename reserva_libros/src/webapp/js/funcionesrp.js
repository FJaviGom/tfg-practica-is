//Petición AJAX GET
//peticion - URL de la solicitud HTTP
//funcionRespuesta - función a llamar cuando termine la petición
//parametro - parámetro adicional que hay que enviar a la función para que
// esta sepa a qué petición se refiere
function ajax(peticion, funcionRespuesta, parametro) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",peticion,true);
	xmlhttp.onreadystatechange = function(){ 
		if ( xmlhttp.readyState==4 ) { 
			if (xmlhttp.status==200) {
				//Respuesta recibida completamente (4) y sin
				//errores del servidor (codigo HTTP 200) 
				//Cambiamos página con la respuesta
				funcionRespuesta(xmlhttp, parametro);
			} else {
				funcionRespuesta(null, parametro);
			}
			
		}
	  };
	
	xmlhttp.send(); //enviamos
}

//Petición AJAX POST
//peticion - URL de la solicitud HTTP
//datosPost - Parámetros a pasar mediante POST (tipo formulario)
//funcionRespuesta - función a llamar cuando termine la petición
//parametro - parámetro adicional que hay que enviar a la función para que
//esta sepa a qué petición se refiere
function ajaxPost(peticion, datosPost, funcionRespuesta, parametro) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST",peticion,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange = function(){ 
		if ( xmlhttp.readyState==4 ) { 
			if (xmlhttp.status==200) {
				//Respuesta recibida completamente (4) y sin
				//errores del servidor (codigo HTTP 200) 
				//Cambiamos página con la respuesta
				funcionRespuesta(xmlhttp, parametro);
			} else {
				funcionRespuesta(null, parametro);
			}
			
		}
	  };
	
	xmlhttp.send(datosPost); //enviamos
}

//Función para acortar las llamadas a getElementById
function elem(id) {
	return document.getElementById(id); 
}


function procesarBorrado(boton) {
	//Buscamos la fila "fila-IDL" y la eliminamos
	var fila = elem("fila-"+boton.value);
	if (fila != null ) {
		fila.parentNode.removeChild(fila);
	}
}

function procesarReserva(boton) {
	//Buscamos la celda "accion-IDL" y la ponemos no disponible
	var celda = elem("accion-"+boton.value);
	if (celda != null ) {
		celda.innerHTML="Reservado";
	}
}

function resultadoReserva(xmlHttp, parametro) {
	var texto = "";
	if (xmlHttp == null) {
		//Envío incorrecto
		texto="-2 ERROR: AJAX";
	} else {
		texto = xmlHttp.responseText.trim();
		if (texto == "-1") {
			texto +=" ERROR: NO IMPLEMENTADO";
		} else if (texto == "0") {
			texto +=" CORRECTO";
			if (parametro != null) {
				//Si era un botón borrar, llamamos a procesar borrado
				if (parametro.classList.contains('bBorrar')) {
					procesarBorrado(parametro);
				} else if (parametro.classList.contains('bReservar')){
					//Si era un botón reservar, llamados a procesar reserva
					procesarReserva(parametro);
				}
			}
			
		} else if (texto == "1") {
			texto +=" ERROR: SQL";
		} else {
			texto +=" ERROR: DESCONOCIDO";
		}
	}
	texto +="\nISBN= "+parametro.value;
	
	
	alert(texto);
}

//Valida el formulario de insertar
function procesaInsertar() {
	var isbn = elem("isbn").value;
	var titulo = elem("titulo").value;
	var autor = elem("autor").value;
	var editorial = elem("editorial").value;
	var anio_pub = elem("anio_pub").value;
	var genero = elem("genero").value;
	var observaciones = elem("observaciones").value;
	
	var resultado = true;
	
	// Comprobaciones inserción campos vacíos
	
	// Comprobación vacío isbn
	resultado = compruebaVacio(isbn,"ISBN");
	
	// Comprobación vacío titulo
	resultado = compruebaVacio(titulo,"Título");
	
	// Comprobación vacío autor
	resultado = compruebaVacio(autor,"Autor");

	// Comprobación vacío editorial
	resultado = compruebaVacio(editorial,"Editorial");
	
	// Comprobación vacío titulo
	resultado = compruebaVacio(anio_pub,"Año de publicación");
	
	// Comprobación vacío genero
	resultado = compruebaVacio(genero,"género");
	
	// Comprobación de selección de un género de la lista
	if(genero == "0" || genero == ''){
		genero = "";
		resultado = false;
		alert("Seleccione un género");
	}
	
	if (resultado) {
		var peticion = "insertar_libro.jsp";
		var datosPost="isbn="+encodeURIComponent( isbn ) +
		"&titulo="+encodeURIComponent( titulo ) +
		"&autor="+encodeURIComponent( autor ) +
		"&editorial="+encodeURIComponent( editorial ) +
		"&anio_pub="+encodeURIComponent( anio_pub ) +
		"&genero="+encodeURIComponent( genero ) +
		"&observaciones="+encodeURIComponent( observaciones );

		//Enviamos
		ajaxPost(peticion, datosPost,resultadoReserva,elem("isbn"));
	}
	
	// Siempre se devuelve false, para que no se envíe directamente,
	// ya que se hace por AJAX
	return false;
}

function enviarReserva(pagina, boton) {
	var isbn = boton.value;
	//Generamos la URL de la petición
	//IMPORTANTE, los campos se envían codificados en base64, para que los
	// caracteres no ascii no den problemas
	var peticion = pagina;
	var datosPost="isbn="+isbn;
	//Enviamos la petición. El resultado se informará a la función resultadoEnvio
	ajaxPost(peticion, datosPost, resultadoReserva, boton); 
	
}

window.onload = function() {
	//	Tareas a realizar al principio.
	// Generar peticiones AJAX en cada boton
	//Botones bBorrar
	var listaBorrar = document.querySelectorAll(".bBorrar");
	for (var boton in listaBorrar) {
		listaBorrar[boton].onclick = function() {
			enviarReserva( "borrar.jsp", this ); 
		  };
	}
	//Botones bReservar
	var listaReservar = document.querySelectorAll(".bReservar");
	for (var boton in listaReservar) {
		listaReservar[boton].onclick = function() { 
			enviarReserva( "reservar.jsp", this );
		  };
	}
	//formulario de inserción
	var formulario = elem("formcrear");
	if (formulario != null) {
		formulario.onsubmit  = procesaInsertar;
	}
}
