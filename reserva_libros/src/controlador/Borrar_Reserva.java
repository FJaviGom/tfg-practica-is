package controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.*;

/**
 * Servlet implementation class Borrar_Reserva
 */
@WebServlet("/borrar_reserva")
public class Borrar_Reserva extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Borrar_Reserva() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long isbn = Long.parseLong(request.getParameter("isbn"));

	//	String dni = request.getParameter("dni");
		
		Reserva reserva_borrar = new Reserva(isbn);
		
		if (reserva_borrar.borrar_Reserva(isbn) > 0){
			request.getRequestDispatcher("borrar_reserva_correcto.jsp").forward(request, response);
		}
		else {
			request.getRequestDispatcher("borrar_reserva_error.jsp").forward(request, response);			
		}

	}

}
