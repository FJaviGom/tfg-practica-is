package controlador;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Libro;
import modelo.Reserva;

/**
 * Servlet implementation class Listar_Libros
 */
//@WebServlet(description = "Servlet controlador encargado de gestionar el listado de todos los libros", urlPatterns = { "/listado_libros" })
public class Listar_Libros extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Listar_Libros() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Libro libro = new Libro();

		ArrayList<Libro> libros = libro.listar_Libros();

		Integer num_libros= libros.size();
		
		Reserva reserva = new Reserva();

		ArrayList<Reserva> reservas = reserva.listar_Reservas();
		
		request.getSession().setAttribute("libros", libros);
		request.getSession().setAttribute("num_libros", num_libros);		
		request.getSession().setAttribute("reservas", reservas);
		
		
		request.getRequestDispatcher("listar_libros.jsp").forward(request, response);

	}

}
