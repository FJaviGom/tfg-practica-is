<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="modelo.Cliente"%>

<%-- Comprobacion de login correcto. Insertamos login.jsp --%>
<%@include file="login.jsp" %>


<!DOCTYPE html>
<html>

  <head>
    <title>Reserva de libros</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/estilorp.css" />
  </head>

  <body> 
    <div id="cabecera">
		<div id="logotipo">
				<a href="datos.jsp">
				<img src="images/logoDep.jpg" alt="Departamento de Ingeniería Telemática" />
				</a>
		</div>
	</div>

	<div id="menu">
		<%-- Insertamos menu.jsp --%>
		<jsp:include page="menu.jsp"></jsp:include>
	</div>
	
	<div id="contenido">
	  <h1>Datos del cliente</h1>
	  <div id="datos" class="${cookie.estilo.value ? 'estiloAlternativo' : '' }">
		<ul>
			<li>Dni:  
				<span id="d_dni" class="datovalor">
					${cliente.dni }
				</span></li>
			<li>Cliente: 
				<span id="d_cliente" class="datovalor">
					${cliente.cliente }
				</span></li>
			<li>Nombre:  
				<span id="d_nombre" class="datovalor">
					${cliente.nombre }
				</span></li>
			<li>Primer apellido:  
				<span id="d_apellido1" class="datovalor">
					${cliente.apellido1 }
				</span></li>
			<li>Segundo apellido:  
				<span id="d_apellido2" class="datovalor">
					${cliente.apellido2 }
				</span></li>
			<li>Dirección:  
				<span id="d_direccion" class="datovalor">
					${cliente.direccion }
				</span></li>
			<li>Administrador: 
				<span id="d_admin" class="datovalor">
					${cliente.admin ? "SÍ" : "NO" }
				</span></li>
		</ul>
	  </div>
	</div>

  </body>
</html>