package controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.*;

/**
 * Servlet implementation class Reservar_Libro
 */
@WebServlet(name = "reservar_libro", urlPatterns = { "/reservar_libro" })
public class Reservar_Libro extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Reservar_Libro() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
/*
		// Obtener dni de usuario con la sesion activa para reservas
		HttpSession sesion_activa = request.getSession();
		Integer dni = (Integer)sesion_activa.getAttribute("dni_cliente_activo");
*/

		Long isbn = Long.parseLong(request.getParameter("isbn"));
//		Integer dni = Integer.parseInt(request.getParameter("dni"));

		//Obtener la instancia del objeto Cliente creado en la autenticacion del cliente en login.jsp a traves del Bean Cliente
		Cliente cliente = (Cliente) request.getSession().getAttribute("cliente");
		Integer dni = cliente.getDni();

		Reserva reserva_libro = new Reserva(isbn,dni);

		reserva_libro.reservar_Libro();

		request.getRequestDispatcher("reserva_libro_correcto.jsp").forward(request, response);
	}

}
