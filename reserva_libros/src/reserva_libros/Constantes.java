package reserva_libros;

public class Constantes {
	private static String DB_ADDR = System.getenv("DB_ADDR") != null ? System.getenv("DB_ADDR") : "postgres";
	private static String DB_PORT = System.getenv("DB_PORT") != null ? System.getenv("DB_PORT") : "5432";

	public static final String DB_DRIVER = "org.postgresql.Driver";
	public static final String DB_URL = "jdbc:postgresql://" + DB_ADDR + ":" + DB_PORT + "/reserva_libros";
	public static final String DB_USER = "dit";
	public static final String DB_PASS = "dit";
}
